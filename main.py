import pygame
import random


class Game:
    def __init__(self, width, height, fps, game_over=False):
        self.width = width
        self.height = height
        self.fps = fps
        self.game_over = game_over
        pygame.init()
        self.surface = pygame.display.set_mode((width, height))
        self.clock = pygame.time.Clock()

    def draw(self):
        self.surface.blit(self.field.background_image, (0, 0))
        for i in range(10):
            self.surface.blit(self.field.name_ship[i].img, (self.field.name_ship[i].x, self.field.name_ship[i].y))
        pygame.display.update()
        self.clock.tick(self.fps)

    def handle(self):       #Управление
        for i in pygame.event.get():
            if i.type == pygame.QUIT:
                exit()
            elif i.type == pygame.KEYDOWN:
                try:
                    if i.key == pygame.K_DOWN:
                        self.field.move_down(self.field.active)     # Организация передвижения корабля вниз по поля
                    elif i.key == pygame.K_UP:
                        self.field.move_up(self.field.active)       # Организация передвижения корабля вверх по поля
                    elif i.key == pygame.K_LEFT:
                        self.field.move_left(self.field.active)     # Организация передвижения корабля влево по поля
                    elif i.key == pygame.K_RIGHT:
                        self.field.move_right(self.field.active)    # Организация передвижения корабля вправо по поля
                    elif i.key == pygame.K_SPACE:
                        self.field.base_location()      # Задание первоначальных значений на поле
                except:
                    print('we have a trouble')

    def start(self):
        self.field = BattleField()      #Создание объекта поле
        self.field.create_ship()

    def run(self):      #Запуск игры и ее исполнение в цикле
        self.start()
        while not self.game_over:
            self.draw()
            self.handle()


class Objects:      #свойства объектов(кораблей)
    def __init__(self, x, y, img):
        self.x = x
        self.y = y
        self.img = img
        self.i_pos = 1
        self.j_pos = 1

    def one_ship(self):
        pass


class BattleField:
    def __init__(self, step=39):
        self.background_image = pygame.image.load('image/background.jpg')
        self.field = [[0 for j in range(12)] for i in range(12)]
        self.step = step
        self.active = 10
        self.name_ship = []

    def create_ship(self):
        x = 534
        for i in range(4):
            self.name_ship.append(Objects(x + self.step*(i+1)*2, 150, pygame.image.load('image/one.jpg')))    # Создание однопалубных кораблей
        for i in range(3):
            self.name_ship.append(Objects(x + self.step*(i+1)*3 - self.step, 225, pygame.image.load('image/two.jpg')))    # Создание двухпалубных кораблей
        for i in range(2):
            self.name_ship.append(Objects(x - self.step + self.step*(i+1)*4 - self.step, 303, pygame.image.load('image/three.jpg')))    # Создание трехпалубных кораблей
        self.name_ship.append(Objects(613, 380, pygame.image.load('image/four.jpg')))    # Создание четырехпалубного кораблей
        print('Объекты созданы')

    def create_bit(self):       # Заполнение матрицы 
        i = self.name_ship[self.active].i_pos
        j = self.name_ship[self.active].j_pos

        if self.active >= 0 and self.active <= 3 and sum(self.field[i][j-1:j+2]) + sum(self.field[i-1][j-1:j+2]) + sum(self.field[i+1][j-1:j+2 ]) == 0:
            self.field[i][j] = 1
            return True
        elif self.active >= 4 and self.active <= 6 and sum(self.field[i][j-1:j+3]) + sum(self.field[i-1][j-1:j+3]) + sum(self.field[i+1][j-1:j+3]) == 0:
            self.field[i][j] = 1
            self.field[i][j+1] = 1
            return True
        elif (self.active == 7 or self.active == 8) and sum(self.field[i][j-1:j+4]) + sum(self.field[i-1][j-1:j+4]) + sum(self.field[i+1][j-1:j+4]) == 0:
            self.field[i][j] = 1
            self.field[i][j+1] = 1
            self.field[i][j+2] = 1
            return True
        elif self.active == 9:
            self.field[i][j] = 1
            self.field[i][j + 1] = 1
            self.field[i][j + 2] = 1
            self.field[i][j + 3] = 1
            return True
        else:
            return False

    def view_matrix(self):
        for i in range(10):
            for j in range(10):
                print(self.field[i][j], end=' ')
            print()
        print('___________________________')

    def base_location(self):    # Расположение корабля в начальной точке(левый верхний угол поля)
        if self.active == 10:
            self.active -= 1
            self.name_ship[self.active].x = 105
            self.name_ship[self.active].y = 148
        elif self.active >= 0:
            if self.create_bit():
                self.active -= 1
                if self.active == -1:
                    self.background_image = pygame.image.load('image/background_2.jpg')
                self.name_ship[self.active].x = 105
                self.name_ship[self.active].y = 148

    def move_down(self, active):
        if self.name_ship[active].y <= 490 and self.active >=0:
            self.name_ship[active].y += 39
            self.name_ship[active].i_pos += 1

    def move_up(self, active):
        if self.name_ship[active].y >= 150 and self.active >=0:
            self.name_ship[active].y -= 39
            self.name_ship[active].i_pos -= 1

    def move_left(self, active):
        if self.name_ship[active].x >= 106 and self.active >=0:
            self.name_ship[active].x -= 39
            self.name_ship[active].j_pos -= 1

    def move_right(self, active):
        if self.name_ship[active].x <= 320 and active == 9 or self.name_ship[active].x <= 360 and active >= 7 and active <= 8 or self.name_ship[active].x <= 410 and active >= 4 and active <= 6 or self.name_ship[active].x <= 430 and active <= 3 and self.active >=0:
            self.name_ship[active].x += 39
            self.name_ship[active].j_pos += 1


class Enemy:
    def __init__(self):
        self.name_enemy_ship = []
        self.active_enemy = 9

    def create_enemy(self):
        pass

    
g = Game(1000, 650, 60)
g.run()
